package com.petermyren.messageboard.messages;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MessageDontBelongToYouException extends RuntimeException {
    
    public MessageDontBelongToYouException(String message) {
        super(message);
    }
}
