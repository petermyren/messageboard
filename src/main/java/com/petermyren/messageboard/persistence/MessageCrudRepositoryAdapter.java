package com.petermyren.messageboard.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageCrudRepositoryAdapter extends CrudRepository<Message, Long> {
}
