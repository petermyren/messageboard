package com.petermyren.messageboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.petermyren.messageboard.users.UserDetailsRepository;

@SpringBootApplication
public class MessageboardApplication {

	private static UserDetailsRepository userDetailsRepository;

	public static void main(String[] args) {
		SpringApplication.run(MessageboardApplication.class, args);
	}

}
