package com.petermyren.messageboard.mapping;

import org.springframework.stereotype.Component;
import com.petermyren.messageboard.messages.Message;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class MessageMapper {

    public List<Message> mapFromEntitiesToMessages(Iterable<com.petermyren.messageboard.persistence.Message> messagesFromDb) {
        return StreamSupport
                .stream(messagesFromDb.spliterator(), false)
                .map(entity -> MessageMapperMapstruct.INSTANCE.entityToMessage(entity))
                .collect(Collectors.toList());
    }

    public com.petermyren.messageboard.persistence.Message mapToEntity(Message messageToSave) {
        return MessageMapperMapstruct.INSTANCE.messageToEntity(messageToSave);
    }

    public Message mapFromEntityToMessage(com.petermyren.messageboard.persistence.Message messageEntity) {
        return MessageMapperMapstruct.INSTANCE.entityToMessage(messageEntity);
    }
}
