package com.petermyren.messageboard.users;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.petermyren.messageboard.persistence.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
