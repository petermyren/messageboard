package com.petermyren.messageboard.users;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.petermyren.messageboard.persistence.User;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    private final UserDetailsRepository userDetailsRepository;

    public JwtUserDetailsService(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       return userDetailsRepository.findByUsername(username).orElseThrow(
               () -> new UsernameNotFoundException("No user found with username " + username)
       );
    }

    public Iterable<User> findAll() {
        return userDetailsRepository.findAll();
    }
}
