package com.petermyren.messageboard.users;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.petermyren.messageboard.security.JwtSecurityRequest;
import com.petermyren.messageboard.security.JwtSecurityResponse;
import com.petermyren.messageboard.security.JwtTokenProvider;

@Slf4j
@RestController
public class UserAuthenticationController {

    private final JwtUserDetailsService userService;
    private final AuthenticationManager authenticationManagerBean;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    public UserAuthenticationController(JwtUserDetailsService service, AuthenticationManager authenticationManagerBean, JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder) {
        this.userService = service;
        this.authenticationManagerBean = authenticationManagerBean;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<JwtSecurityResponse> createAuthenticationToken(@RequestBody JwtSecurityRequest authenticationRequest) throws Exception {
        JwtSecurityResponse authenticationResponse = authenticate(authenticationRequest);
        return ResponseEntity.ok(authenticationResponse);
    }

    private JwtSecurityResponse authenticate(JwtSecurityRequest authenticationRequest) {
        String username = authenticationRequest.getUsername();
        String password = authenticationRequest.getPassword();
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, password);
        authenticationManagerBean.authenticate(authentication);
  
        UserDetails userDetails = this.userService.loadUserByUsername(username);
        String jwt = jwtTokenProvider.generateToken(userDetails);

        return  JwtSecurityResponse.builder().jwttoken(jwt).build();
    }


}

