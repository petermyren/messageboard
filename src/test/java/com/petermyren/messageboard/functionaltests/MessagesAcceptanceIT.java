package com.petermyren.messageboard.functionaltests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MessagesAcceptanceIT {

    @Autowired
    private MockMvc mockMvc;
    
    @Test
    @WithMockUser
    public void getMessages_messagesExists_allMessagesFetched() throws Exception {
        
        // GIVEN
        String expectedMessageList = "" +
                "[" +
                    "{\"id\":1,\"title\":\"My first message\",\"text\":\"First message text here\"}," +
                    "{\"id\":2,\"title\":\"My second message\",\"text\":\"Second message text here\"}" +
                "]";
        
        // WHEN
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/messages")
                        .accept(MediaType.APPLICATION_JSON))
        // THEN
                .andExpect(status().isOk())
                .andExpect(content().json(expectedMessageList));
        
    }

    @Test
    @WithMockUser("testuser1")
    public void saveMessage_messageValid_messageSaved() throws Exception {

        // GIVEN
        String messageToSave = "{\"title\":\"My third message\",\"text\":\"Third message\"}";

        // WHEN
        String expectedMessage = "{\"id\":3,\"author\":\"testuser1\",\"title\":\"My third message\",\"text\":\"Third message\"}";
        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/messages")
                        .with(csrf())
                        .content(messageToSave)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // THEN
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedMessage));

    }
    
    @Test
    @WithMockUser("testuser2")
    public void updateMessage_anotherUsersMessage_badRequest() throws Exception {
        // GIVEN
        String messageToUpdate = "{\"id\":1, \"title\":\"My updated message\",\"text\":\"Updated message\"}";
        
        // WHEN
        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/messages")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf())
                .content(messageToUpdate))
                // THEN
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser("testuser2")
    public void deleteMessage_anotherUsersMessage_badRequest() throws Exception {
        // WHEN
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/messages/1")
                        .with(csrf()))
                // THEN
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser("testuser1")
    public void deleteMessage_usersOwnMessage_statusOk() throws Exception {
        // GIVEN
        allTwoMessagesExist();

        // WHEN
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/messages/1")
                        .with(csrf()))
                // THEN
                .andExpect(status().isOk());
                //AND
                oneMessageIsDeleted();

        
    }

    @Test
    public void saveMessage_notLoggedIn_responseCodeNotAccessable() throws Exception {

        // GIVEN
        String messageToSave = "{\"title\":\"My forth message\",\"text\":\"Forth message\"}";

        // WHEN
        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/messages")
                        .with(csrf())
                        .content(messageToSave)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                // THEN
                .andExpect(status().is4xxClientError());

    }

    private void allTwoMessagesExist() throws Exception {
        String expectedMessageList = "" +
                "[" +
                "{\"id\":1,\"title\":\"My first message\",\"text\":\"First message text here\"}," +
                "{\"id\":2,\"title\":\"My second message\",\"text\":\"Second message text here\"}" +
                "]";
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/messages")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedMessageList));
    }

    private void messageIsUpdated() throws Exception {
        String expectedMessageList = "" +
                "[" +
                "{\"id\":1,\"title\":\"My updated message\",\"text\":\"Updated message\"}," +
                "{\"id\":2,\"title\":\"My second message\",\"text\":\"Second message text here\"}" +
                "]";
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/messages")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedMessageList));
    }

    private void oneMessageIsDeleted() throws Exception {
        String expectedMessageList = "" +
                "[" +
                "{\"id\":2,\"title\":\"My second message\",\"text\":\"Second message text here\"}" +
                "]";
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/messages")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedMessageList));
    }    
}
