package com.petermyren.messageboard.persistence;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import com.petermyren.messageboard.mapping.MessageMapper;
import com.petermyren.messageboard.messages.MessageDontBelongToYouException;
import com.petermyren.messageboard.messages.MessageDontExistException;
import com.petermyren.messageboard.messages.MessageRepository;
import com.petermyren.messageboard.users.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MessageRepositoryTest {
    
    private MessageCrudRepositoryAdapter messageCrudrepoAdapter = mock(MessageCrudRepositoryAdapter.class);
    private MessageMapper messageMapper = new MessageMapper();
    private UserRepository userRepository = mock(UserRepository.class);
    private MessageRepository messageRepository = new MessageRepository(messageMapper, messageCrudrepoAdapter, userRepository);
    private PodamFactory podam = new PodamFactoryImpl();

    @Test
    public void context_loads() {
        assertNotNull("Could not find message repository to run tests for", messageRepository);
    }
    
    @Test
    public void findAll_crudRepoAdapterFindsTwo_messageRepositoryReturnsTwo() {
        // GIVEN
        ArrayList<Message> messages = mockMessagesFromDatabase();

        // WHEN
        List<com.petermyren.messageboard.messages.Message> messagesFound = messageRepository.findAll();
        
        // THEN
        assertEquals("Message repository did not return same amount of messages as fetched from db", messages.size(), messagesFound.size());
    }
    
    @Test
    public void findAll_crudRepoAdapterFindsTwo_messageContainsCorrectId() {
        // GIVEN
        ArrayList<Message> messages = mockMessagesFromDatabase();

        // WHEN
        List<com.petermyren.messageboard.messages.Message> messagesFound = messageRepository.findAll();
        
        // THEN
        assertEquals("Message repository did not return message with correct id", messages.get(0).getId(), messagesFound.get(0).getId());
    }
    
    @Test
    public void findAll_crudRepoAdapterFindsTwo_messageContainsCorrectTitle() {
        // GIVEN
        ArrayList<Message> messages = mockMessagesFromDatabase();

        // WHEN
        List<com.petermyren.messageboard.messages.Message> messagesFound = messageRepository.findAll();
        
        // THEN
        assertEquals("Message repository did not return message with correct title", messages.get(0).getTitle(), messagesFound.get(0).getTitle());
    }
    
    @Test
    public void findAll_crudRepoAdapterFindsTwo_messageContainsCorrectText() {
        // GIVEN
        ArrayList<Message> messages = mockMessagesFromDatabase();

        // WHEN
        List<com.petermyren.messageboard.messages.Message> messagesFound = messageRepository.findAll();
        
        // THEN
        assertEquals("Message repository did not return message with correct message text", messages.get(0).getText(), messagesFound.get(0).getText());
    }

    @Test
    public void createMessage_validMessage_messageSavedContainsTitle() {
        // GIVEN
        com.petermyren.messageboard.messages.Message messageToSave = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        Message savedMessage = mockMessageSaved(messageToSave, "someuser");

        ArgumentCaptor<Message> messageSavedCaptor = ArgumentCaptor.forClass(Message.class);
        when(messageCrudrepoAdapter.save(messageSavedCaptor.capture())).thenReturn(savedMessage);

        // WHEN
        messageRepository.save(messageToSave, "someuser");
        Message messageSaved = messageSavedCaptor.getValue();

        // THEN
        assertEquals("Message repository did not save correct message title", messageToSave.getTitle(), messageSaved.getTitle());
    }

    private Message mockMessageSaved(com.petermyren.messageboard.messages.Message messageToSave, String username) {
        User mockUser = new User();
        mockUser.setUsername(username);
        Message savedMessage = new Message();
        savedMessage.setText(messageToSave.getText());
        savedMessage.setTitle(messageToSave.getTitle());
        savedMessage.setAuthor(mockUser);
        return savedMessage;
    }

    @Test
    public void createMessage_validMessage_messageSavedContainsText() {
        // GIVEN
        String username = "someuser";
        User mockUserEntity = new User();
        mockUserEntity.setId(1L);
        mockUserEntity.setUsername(username);
        mockUserEntity.setPassword("somepass");
        com.petermyren.messageboard.messages.Message messageToSave = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        Message savedMessage = mockMessageSaved(messageToSave, username);

        ArgumentCaptor<Message> messageSavedCaptor = ArgumentCaptor.forClass(Message.class);
        when(messageCrudrepoAdapter.save(messageSavedCaptor.capture())).thenReturn(savedMessage);
        when(userRepository.findByUsername(username)).thenReturn(mockUserEntity);

        // WHEN
        messageRepository.save(messageToSave, username);
        Message messageSaved = messageSavedCaptor.getValue();

        // THEN
        assertEquals("Message repository did not save correct message text", messageToSave.getText(), messageSaved.getText());
    }

    @Test
    public void createMessage_validMessage_messageSavedContainsAuthor() {
        // GIVEN
        com.petermyren.messageboard.messages.Message messageToSave = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        String username = "someuser";
        User mockUserEntity = new User();
        mockUserEntity.setId(1L);
        mockUserEntity.setUsername(username);
        mockUserEntity.setPassword("somepass");
        Message savedMessage = mockMessageSaved(messageToSave, username);

        ArgumentCaptor<Message> messageSavedCaptor = ArgumentCaptor.forClass(Message.class);
        when(messageCrudrepoAdapter.save(messageSavedCaptor.capture())).thenReturn(savedMessage);
        when(userRepository.findByUsername(username)).thenReturn(mockUserEntity);

        // WHEN
        com.petermyren.messageboard.messages.Message messageFromSave = messageRepository.save(messageToSave, username);
        Message messageSaved = messageSavedCaptor.getValue();

        // THEN
        assertNotNull("Message repository did not save correct message author", messageSaved.getAuthor().getUsername());
        assertEquals("Message repository did not save correct message author", username, messageSaved.getAuthor().getUsername());
        assertEquals("Message repository did not save correct message author", username, messageFromSave.getAuthor());
    }

    @Test
    public void updateMessage_messageDontExist_Exception() {
        // GIVEN
        Exception expectedException = null;
        com.petermyren.messageboard.messages.Message messageToUpdate = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        when(messageCrudrepoAdapter.findById(anyLong())).thenReturn(Optional.empty());

        // WHEN
        try {
            messageRepository.updateMessage(messageToUpdate, "myuser");
        } catch (MessageDontExistException mdee) {
            expectedException = mdee;
        }
        // THEN
        assertNotNull("Expected mesagedontexistexception when trying to update non existing message", expectedException);
    }

    @Test
    public void updateMessage_otherUsersMessage_Exception() {
        // GIVEN
        Exception expectedException = null;
        com.petermyren.messageboard.messages.Message messageToUpdate = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        User mockUserEntity = mockMessageOwner();
        mockAnotherUserOwnsMessage(messageToUpdate, mockUserEntity);

        // WHEN
        try {
            messageRepository.updateMessage(messageToUpdate, "myuser");
        } catch (MessageDontBelongToYouException mdbtye) {
            expectedException = mdbtye;
        }
        // THEN
        assertNotNull("Message repository should not allow update on other users message", expectedException);
    }

    @Test
    public void updateMessage_sameUsersMessage_messageSentForUpdate() {
        // GIVEN
        String myuser = "myuser";
        Exception expectedException = null;
        com.petermyren.messageboard.messages.Message messageToUpdate = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        User mockUserEntity = mockMessageOwner();
        mockUserEntity.setUsername(myuser);
        mockSameUserOwnsMessage(messageToUpdate, mockUserEntity);
        ArgumentCaptor<Message> messageSentForUpdate = ArgumentCaptor.forClass(Message.class);
        when(messageCrudrepoAdapter.save(messageSentForUpdate.capture())).thenReturn(mockMessageSaved(messageToUpdate, myuser));

        // WHEN
        try {
            messageRepository.updateMessage(messageToUpdate, myuser);
        } catch (MessageDontBelongToYouException mdbtye) {
            expectedException = mdbtye;
        }
        // THEN
        assertNull("Message repository should allow update users own message", expectedException);
        assertEquals("Expected message to be sent to update when user owned the object, but none deleted",
                messageToUpdate.getId(), messageSentForUpdate.getValue().getId());
    }

    @Test
    public void deleteMessage_otherUsersMessage_Exception() {
        // GIVEN
        Exception expectedException = null;
        com.petermyren.messageboard.messages.Message messageToDelete = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        User mockUserEntity = mockMessageOwner();
        mockAnotherUserOwnsMessage(messageToDelete, mockUserEntity);

        // WHEN
        try {
            messageRepository.delete(messageToDelete.getId(), "myuser");
        } catch (MessageDontBelongToYouException mdbtye) {
            expectedException = mdbtye;
        }
        // THEN
        assertNotNull("Message repository should not allow delete on other users message", expectedException);
    }
    
    @Test
    public void deleteMessage_sameUsersMessage_messageSentForDeletion() {
        // GIVEN
        String myuser = "myuser";
        Exception expectedException = null;
        com.petermyren.messageboard.messages.Message messageToDelete = podam.manufacturePojo(com.petermyren.messageboard.messages.Message.class);
        User mockUserEntity = mockMessageOwner();
        mockUserEntity.setUsername(myuser);
        mockSameUserOwnsMessage(messageToDelete, mockUserEntity);
        ArgumentCaptor<Message> messageSentForDeletion = ArgumentCaptor.forClass(Message.class);
        doNothing().when(messageCrudrepoAdapter).delete(messageSentForDeletion.capture());
        
        // WHEN
        try {
            messageRepository.delete(messageToDelete.getId(), myuser);
        } catch (MessageDontBelongToYouException mdbtye) {
            expectedException = mdbtye;
        }
        // THEN
        assertNull("Message repository should allow delete users own message", expectedException);
        assertEquals("Expected message to be sent to deletion when user owned the object, but none deleted", 
                messageToDelete.getId(), messageSentForDeletion.getValue().getId());
    }

    private User mockMessageOwner() {
        String username = "otheruser";
        User mockUserEntity = new User();
        mockUserEntity.setId(1L);
        mockUserEntity.setUsername(username);
        mockUserEntity.setPassword("somepass");
        return mockUserEntity;
    }

    private void mockAnotherUserOwnsMessage(com.petermyren.messageboard.messages.Message message, User mockUserEntity) {
        Message mockMessageFromDb = new Message();
        mockMessageFromDb.setAuthor(mockUserEntity);
        Optional<Message> mockMessageFromDbOptional = Optional.of(mockMessageFromDb);
        when(messageCrudrepoAdapter.findById(message.getId())).thenReturn(mockMessageFromDbOptional);
    }
    
    private void mockSameUserOwnsMessage(com.petermyren.messageboard.messages.Message messageToDelete, User mockUserEntity) {
        Message mockMessageFromDb = new Message();
        mockMessageFromDb.setId(messageToDelete.getId());
        mockMessageFromDb.setAuthor(mockUserEntity);
        Optional<Message> mockMessageFromDbOptional = Optional.of(mockMessageFromDb);
        when(messageCrudrepoAdapter.findById(messageToDelete.getId())).thenReturn(mockMessageFromDbOptional);
    }

    private ArrayList<Message> mockMessagesFromDatabase() {
        ArrayList<Message> messages = new ArrayList<>();
        messages.add(podam.manufacturePojo(Message.class));
        messages.add(podam.manufacturePojo(Message.class));
        when(messageCrudrepoAdapter.findAll()).thenReturn(messages);
        return messages;
    }

}